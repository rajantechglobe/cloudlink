<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 
 */

get_header(); ?>

<section class="banner-section">
	
		<?php if( have_rows('service_pages', 5) ): ?>
            <div class="banner-img">
			<?php while( have_rows('service_pages', 5) ): the_row();

			$image = get_sub_field('service_image', 5);					

			$title = get_sub_field('service_name', 5);
			
			$link = get_sub_field('service_link', 5);

			?>
			<span>
			<?php if($image): ?>
			<img src="<?php echo $image['sizes']['service-home-thumb']; ?>" class="img-responsive" alt="<?php echo $title; ?>">
			<?php endif; ?>				
			<?php if($title): ?>
			<a href="<?php echo $link; ?>" class="themebtn thborder"><?php echo $title; ?></a>	
			<?php endif; ?> 
			</span>
            <?php endwhile; ?>
            </div>
		<?php endif; ?> 
			
</section>  <!--banner section end here  -->
<?php get_footer();
