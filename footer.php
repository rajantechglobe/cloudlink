<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>



		<footer id="colophon" class="site-footer" role="contentinfo">
				<div class="container">
						<div class="row">
							<div class="col-sm-6">
							<?php $copyright = get_field('copyright_text',5);?>
							<?php if($copyright): ?><p><?php echo $copyright; ?></p><?php endif; ?>
							</div>
							<div class="col-sm-6">
							<?php $designed = get_field('designed_by_text',5);?>
							<?php if($designed): ?><p class="text-right"><?php echo $designed; ?></p><?php endif; ?>
							</div>	
						</div>
		</footer><!-- #colophon -->
<?php wp_footer(); ?>

</body>
</html>
