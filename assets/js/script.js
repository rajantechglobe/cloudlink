
 function scrollDown() {
    var vheight = $(window).height() - 120;
    $('html, body').animate({
        scrollTop: (Math.floor($(window).scrollTop() / vheight) + 1) * vheight
    }, 500);
};
$(document).ready(function() {
    $('.scroll-down').click(function(event) {
        scrollDown();
        event.preventDefault();
    });

});
 /*scroll to top botton*/


 
 
 
 
 $(document).ready(function() {

 
 
     //Check to see if the window is top if not then display button

     $(window).scroll(function() {

         if ($(this).scrollTop() > 115) {

             $('.scrollToTop').css({

                 "bottom": "20px"

             });

         } else {

             $('.scrollToTop').css({

                 "bottom": "-100px"

             });

         }

     });

     //Click event to scroll to top

     $('.scrollToTop').click(function() {

         $('html, body').animate({

             scrollTop: 0

         }, 800);

         return false;

     });

 });
$(document).ready(function() {
	 $('.clients-carousel').owlCarousel({
		
		items:5,
		autoplay: 3000, //Set AutoPlay to 3 seconds
		autoplaySpeed: 2000,
		navSpeed: 2000,
		loop:true,
		margin:10,
		nav:false,
		dots:true,
		margin:28,
		autoplayHoverPause: true,
		//responsive:{
		 // 0:{
			//items:1
		// },
		//  600:{
		//	  items:2
		//   },
		//  1000:{
	//		 items:3
	//	 }
		//}
	});
});
/* ==============================================

Detect Mobile Devices

=============================================== */

var detectmob = false; 

    if(navigator.userAgent.match(/Android/i)

        || navigator.userAgent.match(/webOS/i)

        || navigator.userAgent.match(/iPhone/i)

        || navigator.userAgent.match(/iPad/i)

        || navigator.userAgent.match(/iPod/i)

        || navigator.userAgent.match(/BlackBerry/i)

        || navigator.userAgent.match(/Windows Phone/i)) {  

            detectmob = true;

    }

	if (detectmob != true) {

   // $( '.about' ).parallax('50%',0.3,false);
    //$( '.testimonial' ).parallax('50%',0.3,false);

    $( '.entry-header, .page-header' ).parallax('50%',0.4,false);

	//$(".about .form-holder").css({'min-height':($(".about").height()+'px')});

}


	
$('.panel-heading a').on('click',function(e){
    if($(this).parents('.panel').children('.panel-collapse').hasClass('in')){
        e.preventDefault();
        e.stopPropagation();
    }
});
	
	
 //For logos section


 $(document).ready(function() {

     "use strict";

     $("nav .custom-logo-link").addClass("navbar-brand").removeClass("custom-logo-link");
	 
	$( "#top-social" ).appendTo( ".navbar-collapse" );
	
     $(".sub-menu").addClass("dropdown-menu");

     $(".menu-item-has-children > a").after('<span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></span>');

    //$(".navbar a, a.themebtn, .services a, .site-footer .menu a"). removeAttr("href");

     $(".carousel.slide").addClass("carousel-fade");
	

	  $('.carousel img[sizes]').each(function() { $(this).removeAttr('sizes'); });

	 $('.carousel img[srcset]').each(function() { $(this).removeAttr('srcset'); });

	$("a[rel^='prettyPhoto']").prettyPhoto({

		social_tools:false,

	  }); 

     $('.widget_wysija').addClass('form-inline');

     $('.wysija-paragraph').addClass('form-group');

     $('.wysija-input').addClass('form-control');

     $('.wysija-submit').addClass('btn themebtn');

//	 $(" input.form-control").wrap("<div class='input-group'></div>");

//	   $("input[type=text]").after("<div class='input-group-addon'><i class='fa fa-user'></i></div>");

//	   $(" input[type=email]").after("<div class='input-group-addon'><i class='fa fa-envelope-o'></i></div>");

//	   $(" input[type=tel]").after("<div class='input-group-addon'><i class='fa fa-phone'></i></div>");

     $(".entry-content .attachment-post-thumbnail").addClass("img-responsive center-block img-thumbnail");

	 //transition delay in menu

	 var drop = $(' ul.sub-menu > li');

    $('li.menu-item-has-children').each(function () {

        var delay = 0;

        $(this).find(drop).each(function () {

            $(this).css({

                transitionDelay: delay + 'ms'

            });

            delay += 40;

        });

    });

 });

 /* Demo Scripts for Bootstrap Carousel and Animate.css article*/

 (function($) {

     //Function to animate slider captions 

     function doAnimations(elems) {

         //Cache the animationend event in a variable

         var animEndEv = 'webkitAnimationEnd animationend';

         elems.each(function() {

             var $this = $(this),

                 $animationType = $this.data('animation');

             $this.addClass($animationType).one(animEndEv, function() {

                 $this.removeClass($animationType);

             });

         });

     }

     //Variables on page load 

    // var $myCarousel = $('.carousel.slide'),

         $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");

     //Initialize carousel 

     $myCarousel.carousel();

     //Animate captions in first slide on page load 

     doAnimations($firstAnimatingElems);

     //Pause carousel  

     $myCarousel.carousel('pause');

     //Other slides to be animated on carousel slide event 

     $myCarousel.on('slide.bs.carousel', function(e) {

         var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");

         doAnimations($animatingElems);

     });

 })(jQuery);